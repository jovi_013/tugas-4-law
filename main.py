from fastapi import FastAPI, Form, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from time import time
from hashlib import sha1
from datetime import datetime


'''
Jovi Handono Hutama
1806205161
LAW A
'''

app = FastAPI()

class Data(BaseModel):
    nama: str
    npm: str

data = {}
data["1806205161"] = Data(nama="Jovi", npm="1806205161")

@app.get("/")
def read_root():
    return "Please see url /docs for documentation."

@app.get("/read/{npm}/")
def read(npm: str):
    _data = data[npm]

    response = {
      "status": "OK",
      "npm": _data.npm,
      "nama": _data.nama,
    }

    return JSONResponse(content=jsonable_encoder(response))

@app.post("/update/")
def update(payload: Data):
    _data = data[payload.npm]

    _data.npm = payload.npm
    _data.nama = payload.nama

    data[payload.npm] = _data

    return JSONResponse(content=jsonable_encoder({"status": "OK"}))